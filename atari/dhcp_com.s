  .include "..\inc\common.i"
  .include "..\inc\commonprint.i"
  .include "..\inc\net.i"
  
  .import dns_set_hostname
  .import dns_resolve
  .import dns_ip
  .import dns_status
  .import cfg_get_configuration_ptr

  .import copymem
  .importzp copy_src
  .importzp copy_dest
  .import icmp_echo_ip
  .import icmp_ping
  .import check_for_abort_key
  .import parse_integer
  .import strlen
  .import timer_unhook


  .import _arg1
  .import _arg2
  .import _arg3
  .import _argcount
  .import timer_unhook
  
  .export _aip_main
  .export output_buffer

.bss
  output_buffer: .res 520
  resolved_ip:   .res 4
  loop_ctr:      .res 1
  
.CODE

; expects pointer to name in ax
; returns pointer to resolved ip in ax
;         carry set = error
;         carry clear = ok
resolve:
  jsr dns_set_hostname
  jsr dns_resolve
rts

_aip_main:
  
  init_ip_via_dhcp 
  bcc :+             ; branch if no error
  jsr strout
  .byte "ip stack initialization failure.",155,0
  jmp aip_exit
:  
  jsr print_ip_config

aip_exit:
  jsr timer_unhook
  rts
    







