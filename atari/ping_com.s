  .include "..\inc\common.i"
  .include "..\inc\commonprint.i"
  .include "..\inc\net.i"
  
  .import dns_set_hostname
  .import dns_resolve
  .import dns_ip
  .import dns_status
  .import cfg_get_configuration_ptr

  .import copymem
  .importzp copy_src
  .importzp copy_dest
  .import icmp_echo_ip
  .import icmp_ping
  .import check_for_abort_key
  .import parse_integer
  .import strlen
  .import timer_unhook


  .import _arg1
  .import _arg2
  .import _arg3
  .import _argcount
  .import timer_unhook
  
  .export _aip_main
  .export output_buffer

.bss
  output_buffer: .res 520
  resolved_ip:   .res 4
  loop_ctr:      .res 1
  
.CODE

; expects pointer to name in ax
; returns pointer to resolved ip in ax
;         carry set = error
;         carry clear = ok
resolve:
  jsr dns_set_hostname
  jsr dns_resolve
rts

_aip_main:
  
  init_ip_via_dhcp 
  bcc :+             ; branch if no error
  jsr strout
  .byte "ip stack initialization failure.",155,0
  jmp aip_exit
:  
  ;jsr print_ip_config
  jsr strout
  .byte "resolving ",0
  ldax _arg1
  jsr print
  jsr strout
  .byte "...",155,0
    
  ldax _arg1
  jsr resolve
  bcc :+             ; branch if no error
  jsr strout
  .byte "could not resolve ",0
  ldax _arg1
  jsr print
  jmp aip_exit
:
  ldx #$3
:
  lda  dns_ip,x
  sta  icmp_echo_ip,x
  dex
  bpl :-
    
  ; get arg2 as number of pings
  lda _argcount
  cmp #3
  bcs :+            ; branch on greater than or equal to
    ldy #4          ; __arg is  < 2, default to 4 iterations
    sty loop_ctr
    jmp @pingloop
  :                ; argc is  >= 2      
  ldax _arg2
  jsr parse_integer   ; currently does not set carry if non-integer in ax
  sta loop_ctr        ; so just hope it worked right

  ; start ping loop
@pingloop:
  jsr strout
  .byte "Pinging : ",0
  ldax #icmp_echo_ip
  jsr print_dotted_quad
  jsr strout
  .byte " ",0 
  jsr icmp_ping
  bcs @error
  jsr print_integer
  jsr strout
  .byte " ms",0
  jsr print
  jsr print_cr
  dec loop_ctr
  bne @pingloop
  jmp aip_exit

@error:
  jsr print_errorcode
  jmp aip_exit

aip_exit:
  jsr timer_unhook
  rts
    






