.export print_a
.export strout
.export print_a_inverse
.export print_cr
.export cls
.export beep
.export tempa
.export tempx
.export tempy

.export screen_current_row
.export screen_current_col

.include "a8.i"
.include "a8macros.i"
.setcpu		"65C02"

.bss
ciov_temp: .res 1
tempa: .res 1
tempx: .res 1
tempy: .res 1
screen_current_col: .res 1
screen_current_row: .res 1

.code


;
;use ATARI CIOV function to display 1 char
;inputs: A should be set to ASCII char to display
;outputs: none
ciov_one:
  pushregs
  sta ciov_temp
  LDA #11
  STA ICCOM
  LDA #1			
  STA ICBLL
  LDA #0			
  STA ICBLH
  lda #<ciov_temp
  STA ICBAL
  lda #>ciov_temp
  STA ICBAH
  LDX #0
  JSR CIOV
  pullregs
rts

print_a:
  jsr ciov_one
rts

;inputs: none
;outputs: none
print_cr:
  sta tempa
  LDA #155      ; CR/LF char
  jsr print_a
  lda tempa
  rts

;inputs: none
;outputs: none  
cls:
  sta tempa
  LDA #125     ; clear screen
  jsr print_a
  lda tempa
  rts

;use Apple 2 monitor ROM function to move to make a 'beep' noise
;inputs: none
;outputs: none
beep:
  sta tempa
  LDA #253    ; beep char
  jsr print_a
  lda tempa
  rts
  
print_a_inverse:
  ora  #$80  ; turn on top bit
  JMP print_a


strout:
	 PLA			; get string address from stack
	 TAY
	 PLA
	 TAX
	 INY
	 BNE @NIN2
	 INX
@NIN2:
 	 jsr findlen

	 tay
	 txa
	 pha			; modify return address to 
	 tya			; return immediately after
	 pha			; terminating 0
putbuf:
	 LDA #PUTCHR		; found end of string
	 STA ICCOM
  LDX #0
	 JMP CIOV		; let CIO display it


findlen:
	 STY ICBAL		; point at start of string
	 STY @loop+1
	 STX @loop+2
	 STX ICBAH
	 LDY #0			; no data yet
	 STY ICBLH
@loop:   lda $c000,y		; - this address gets modified
         beq @exit		; continue until we find terminating 0
	 iny
	 bne @loop		; forced branch
@exit:
	 TYA
	 STA ICBLL
	 clc
	 adc @loop+1
	 bcc @noinx
	 inx
@noinx:
	 rts

  



;-- LICENSE FOR a2print.s --
; The contents of this file are subject to the Mozilla Public License
; Version 1.1 (the "License"); you may not use this file except in
; compliance with the License. You may obtain a copy of the License at
; http://www.mozilla.org/MPL/
; 
; Software distributed under the License is distributed on an "AS IS"
; basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
; License for the specific language governing rights and limitations
; under the License.
; 
; The Original Code is ip65.
; 
; The Initial Developer of the Original Code is Jonno Downes,
; jonno@jamtronix.com.
; Portions created by the Initial Developer are Copyright (C) 2009
; Jonno Downes. All Rights Reserved.  
; -- LICENSE END --
