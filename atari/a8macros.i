.ifndef A8MACROS__I__
A8MACROS__I__ = 1

.bss
temp_a: .res 1
.code

;a - char to print
;x - row
;y - column 
; saves & restores cursor pos
.macro print_at row,col,char
  lda #char
  sta tempa 
  lda ROWCRS
  pha
  lda COLCRS
  pha

  lda #row
  sta ROWCRS
  lda #col
  sta COLCRS
  lda tempa
  jsr print_a

  pla
  sta COLCRS
  pla
  sta ROWCRS
.endmacro

.macro save_cursor_xy
  lda ROWCRS
  sta screen_current_row
  lda COLCRS
  sta screen_current_col
.endmacro 

.macro restore_cursor_xy
  lda screen_current_row
  sta ROWCRS
  lda screen_current_col
  sta COLCRS
.endmacro

.macro set_cursor_xy row,col
  lda #row
  sta ROWCRS
  lda #col
  sta COLCRS
.endmacro

.macro print_xy row,col
  phax
  save_cursor_xy
  set_cursor_xy row,col
  plax
  jsr print
  restore_cursor_xy
.endmacro

.macro pushregs
  sta temp_a
  pha 
  txa
  pha
  tya
  pha
  lda temp_a
.endmacro

.macro pullregs
  pla 
  tay
  pla
  tax
  pla
.endmacro

.macro copybytes src,dest,cnt
  ldx #cnt
: lda src,x
  sta dest,x
  dex
  bpl :-
.endmacro



.endif
