  .include "..\inc\common.i"
  .include "..\inc\commonprint.i"
  .include "..\inc\net.i"
  .include "../inc/printf.i"
  .include "a8macros.i"
  
  .import dns_set_hostname
  .import dns_resolve
  .import dns_ip
  .import dns_status
  .import cfg_get_configuration_ptr

  .import copymem
  .import ip_callback
  .importzp copy_src
  .importzp copy_dest
  .import icmp_echo_ip
  .import icmp_ping
  .import check_for_abort_key
  .import parse_integer
  .import strlen
  .import timer_unhook
  .import dbg_dump_eth_header
  .import promiscuous_mode

  .import _arg1
  .import _arg2
  .import _arg3
  .import _argcount
  .import timer_unhook

  .import eth_outp, eth_outp_len
  .import ip_outp
  .import ip_inp
  .import udp_outp
  .import eth_inp
  .import eth_inp_len

  .import udp_add_listener
  .import udp_callback
  .import udp_remove_listener
  .import udp_callback
  .import udp_send
  .import check_for_abort_key
  .import udp_inp
  .import ip_inp
  .importzp ip_src

  .importzp udp_src_port
  .importzp udp_dest_port
  .importzp udp_len
  .importzp udp_cksum
  .importzp udp_data

  .import udp_send_dest
  .import udp_send_src_port
  .import udp_send_dest_port
  .import udp_send_len
  

  .export _aip_main
  .import output_buffer

  server:
  .byte 192,168,2,2

  msg:
  .byte "hi there!",0

  ;udp packet structure
  upacket_size = 2+4+2+2+2
  upacket:    .res upacket_size
  up_length    = 0
  up_srcip     = 2
  up_srcport   = 6
  up_destport  = 8
  up_data      = 10

  ;command packet structure
  cpacket_size = 2+2+256
  cpacket:    .res cpacket_size
  cp_seq       = 0
  cp_type      = 2
  cp_data      = 3
    

.bss
  resolved_ip:   .res 4
  loop_ctr:      .res 1
  ipaddr:        .res 4
  tempint:       .res 2
  port1:         .res 2
  
.CODE

udp_cb:
  jsr strout
  .byte "in udp_cb",155,0

jsr strout
.byte "on port ",0
ldx udp_inp + udp_dest_port
lda udp_inp + udp_dest_port + 1
jsr print_integer
jsr strout
.byte 155,0

jsr strout
.byte "from port ",0
ldx udp_inp + udp_src_port
lda udp_inp + udp_src_port + 1
jsr print_integer
jsr strout
.byte 155,0

jsr strout
.byte "length ",0
ldx udp_inp + udp_len
lda udp_inp + udp_len + 1
jsr print_integer
jsr strout
.byte 155,0

jsr strout
.byte "data ",0
ldax #udp_inp+udp_data
jsr print
jsr strout
.byte 155,0

;printf "udp_dest_port %04x\r"  = 2 ;offset of destination port field in udp packet
;printf "udp_len	      %04x\r"  = 4 ;offset of length field in udp packet
;printf "udp_cksum     %04x\r"  = 6 ;offset of checksum field in udp packet
;printf "udp_data      %04x\r"  = 8 ;offset of data in udp packet


  ldax #256
  stax udp_send_len
  
  ldax port1
  stax udp_send_src_port

  copybytes server,udp_send_dest,3

  ldax port1			; set destination port
  stax udp_send_dest_port
  copybytes msg,output_buffer,9
  ldax #output_buffer
  jsr udp_send
  bcs @error_in_send
  rts
@error_in_send:  
  jsr strout
  .byte "send fail",155,0
  lda #KPR_ERROR_TRANSMIT_FAILED
  sta ip65_error
  sec
rts

_aip_main:
  
  init_ip_via_dhcp 
  bcc :+             ; branch if no error
  jsr strout
  .byte "ip stack initialization failure.",155,0
  jmp aip_exit
:  

  jsr strout
  .byte "listening on ",0
  ldax #cfg_ip
  jsr print_dotted_quad
  jsr strout
  .byte 155,0

  ldax #udp_cb
  stax udp_callback
  ldax #11000
  stax port1
  jsr udp_add_listener
@mainloop:
  jsr ip65_process
  ;bcs @error
  jmp @mainloop

@error:
  jsr print_errorcode
  jmp aip_exit

aip_exit:
  jsr timer_unhook
  rts
    







