  .include "..\inc\common.i"
  .include "..\inc\commonprint.i"
  .include "..\inc\net.i"
  .include "../inc/printf.i"
  .include "a8macros.i"
  
  .import dns_set_hostname
  .import dns_resolve
  .import dns_ip
  .import dns_status
  .import cfg_get_configuration_ptr

  .import copymem
  .import ip_callback
  .importzp copy_src
  .importzp copy_dest
  .import icmp_echo_ip
  .import icmp_ping
  .import check_for_abort_key
  .import parse_integer
  .import strlen
  .import timer_unhook
  .import dbg_dump_eth_header
  .import promiscuous_mode

  .import _arg1
  .import _arg2
  .import _arg3
  .import _argcount
  .import timer_unhook

  .import eth_outp, eth_outp_len
  .import ip_outp
  .import ip_inp
  .import udp_outp
  .import eth_inp
  .import eth_inp_len
  
  .export _aip_main
  .export output_buffer

.bss
  output_buffer: .res 520
  resolved_ip:   .res 4
  loop_ctr:      .res 1
  ipaddr:        .res 4
  tempint:       .res 2
  
.CODE

;prints out the header  of ethernet packet that is ready to be sent;
; inputs: 
; eth_outp: pointer to ethernet packet
; eth_outp_len: length of ethernet packet
; outputs: none
print_packet:
    pushregs
    ;lda ip_outp+14
    ;sta ipaddr
    ;lda ip_outp+15
    ;sta ipaddr+1
    ;lda ip_outp+13
    ;sta ipaddr+2
    ;lda ip_outp+12
    ;sta ipaddr+3
    ldax #ip_inp+12
    jsr print_dotted_quad
    jsr strout
    .byte " ",0
    ldax #ip_inp+16
    jsr print_dotted_quad
    ;lda eth_inp+12
    ;sta tempint+1
    ;lda eth_inp+13
    ;sta tempint
	;printf " t:%04x ", tempint
	;printf " p:%02x", ip_inp + 9
    lda ip_inp + 9
    cmp #17
    bne :+
    jsr strout
    .byte " UDP  ",0
    jmp @done
    :
    cmp #6
    bne :+
    jsr strout
    .byte " TCP  ",0
    jmp @done
    :
    cmp #1
    bne :+
    jsr strout
    .byte " ICMP ",0
    jmp @done
    :
    jsr strout
    .byte "??? ",0
    @done:

	printf "l:%04d", eth_inp_len
	;printf "dest: %04x:%04x:%04x\r", eth_outp, eth_outp + 2, eth_outp + 4
	;printf "src: %04x:%04x:%04x\r", eth_outp + 6, eth_outp + 8, eth_outp + 10
	;printf "ver,ihl,tos: %04x\r", ip_outp
	;printf "len: %04x\r", ip_outp + 2
	;printf "id: %04x\r", ip_outp + 4
	;printf "frag: %04x\r", ip_outp + 6
	;printf "ttl: %02x\r", ip_outp + 8
	;printf "cksum: %04x\r", ip_outp + 10
	;printf "src: %04x%04x\r", ip_outp + 12, ip_outp + 14
	;printf "dest: %04x%04x\r", ip_outp + 16, ip_outp + 18
    jsr strout
    .byte 155,0
    pullregs
	rts


dump_packet:
  ;jsr strout
  ;.byte "got a packet",155,0
  jsr print_packet
  rts

_aip_main:
  
  lda #1
  sta promiscuous_mode ;request promiscuous mode
  init_ip_via_dhcp 
  bcc :+             ; branch if no error
  jsr strout
  .byte "ip stack initialization failure.",155,0
  jmp aip_exit
:  

  jsr strout
  .byte "listening on ",0
  ldax #cfg_ip
  jsr print_dotted_quad
  jsr strout
  .byte 155,0

  ldax #dump_packet
  stax ip_callback
@mainloop:
  jsr ip65_process
  ;bcs @error
  jmp @mainloop

@error:
  jsr print_errorcode
  jmp aip_exit

aip_exit:
  jsr timer_unhook
  rts
    






