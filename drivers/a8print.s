.export print_a
.export print_a_inverse
.export print_cr
.export cls
.export beep
.exportzp screen_current_row
.exportzp screen_current_col

.code

screen_current_col=$55 ; CH - Horizontal cursor-position (0-39)
screen_current_row=$54 ; CV - Vertical cursor-position (0-23)

;
;use ATARI CIOV function to display 1 char
;inputs: A should be set to ASCII char to display
;outputs: none
print_a:
  LDX #11
	 STX 834
	 LDX #0			
	 STX 840
	 STX 841
	 JMP 58454

;inputs: none
;outputs: none
print_cr:
  LDA #155      ; CR/LF char
  JMP print_a

;inputs: none
;outputs: none  
cls:
  LDA #125     ; clear screen
  JMP print_a

;use Apple 2 monitor ROM function to move to make a 'beep' noise
;inputs: none
;outputs: none
beep:
  LDA #253    ; beep char
  JMP print_a
  
print_a_inverse:
  ora  #$80  ; turn on top bit
  JMP print_a


;-- LICENSE FOR a2print.s --
; The contents of this file are subject to the Mozilla Public License
; Version 1.1 (the "License"); you may not use this file except in
; compliance with the License. You may obtain a copy of the License at
; http://www.mozilla.org/MPL/
; 
; Software distributed under the License is distributed on an "AS IS"
; basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
; License for the specific language governing rights and limitations
; under the License.
; 
; The Original Code is ip65.
; 
; The Initial Developer of the Original Code is Jonno Downes,
; jonno@jamtronix.com.
; Portions created by the Initial Developer are Copyright (C) 2009
; Jonno Downes. All Rights Reserved.  
; -- LICENSE END --
