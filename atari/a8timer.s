; timer routines
;
; the timer should be a 16-bit counter that's incremented by about
; 1000 units per second. it doesn't have to be particularly accurate.

	.include "common.i"
        .include "commonprint.i"
    .include "a8.i"

	.export timer_read
	.export timer_init
	.export timer_unhook
	.export timer_read
    .export timer_seconds  ; this should return a single BCD byte (00..59) which is a count of seconds

	.code

initialized:
.byte 0
seconds:
.byte 0
minutes:
.byte 0
hours:
.byte 0
current_time_value:
.byte 0,0
jiffies:
.byte 0
vbichain:
.byte 0,0
scr:
.byte 0,0

  
; initialize timers
timer_init:
  ;jsr strout
  ;.byte "timer_init",155,0
  lda initialized
  bne @installed
  lda #1
  sta initialized
  ; save old immediate vector
  lda $222
  sta vbichain
  lda $223
  sta vbichain+1


  ; now set up new (default NTSC)
                LDX # >timer_inc ;HI-BYTE
                LDY # <timer_inc ;LO-BYTE

        ; Check for PAL GTIA
                lda     PAL			; $d014(r)
                and     #%00001110
                bne     @use_ntsc

        ; Use PAL isr
                ldx     #>timer_inc_PAL
                ldy     #<timer_inc_PAL

@use_ntsc:
  LDA #6      ;STAGE 1 VBI
  JSR SETVBV  ;SET IT
@installed:
  lda #0
  sta current_time_value
  sta current_time_value+1
  rts

; restore original vbi vector
timer_unhook:
  lda #0
  ora vbichain
  ora vbichain+1
  bne :+
  ;timer_init hasn't been called I guess 
  rts 
: ldy vbichain
  ldx vbichain+1
  lda #6
  jsr SETVBV
  rts

timer_seconds:
  lda $14
  rts

; return the current value
timer_read:
  ldax current_time_value
  rts



;-------------------------------------------------------------------------------
;
; @isr:         timer_inc       (VBI for NTSC [60 Hz])
;
; @Description: Advances the time counters.
;               The 16-bit milliseconds timer "current_time_value" is increment
;               by 17, which approximates the 16.666ms of the 60Hz interupt.
;               The 16-bit counter "seconds" is advanced every 60th call.
;
;-------------------------------------------------------------------------------
timer_inc:
  ; this counter holds 
  ; the timer, which should be a 16-bit counter that's incremented by about
  ; 1000 units per second. it doesn't have to be particularly accurate.
  ; it rolls over every 60 seconds or so
  ; it gets re-initialized on every call to init_timer
  lda #17  ; 60 HZ =~ 17 ms per 'tick' 
  clc
  adc current_time_value
  sta current_time_value
  bcc :+
  inc current_time_value+1
:

  ;jiffies counts up jiffies ( 1/60th of a second ) up to 1 second
  inc jiffies
  lda jiffies
  cmp #60
  bne @done
  lda #0
  sta jiffies
  ; and then increments our seconds counter
  ; which rolls over every 18 hours
  lda #1  
  clc
  adc seconds
  sta seconds
  bcc :+
  inc seconds+1  
:
  ldy #0
  lda seconds

;  sta (88),y
;  iny
;  lda seconds+1
;  sta (88),y
@done:
  jmp SYSVBV



;-------------------------------------------------------------------------------
;
; @isr:         timer_inc       (VBI for PAL [50 Hz])
;
; @Description: Advances the time counters.
;               The 16-bit milliseconds timer "current_time_value" is increment
;               by 20.
;               The 16-bit counter "seconds" is advanced every 50th call.
;
;-------------------------------------------------------------------------------
MS_PER_VBI      = 20
JIFFIES_PER_SEC = 50
timer_inc_PAL:
                lda     #MS_PER_VBI
                clc
                adc     current_time_value
                sta     current_time_value
                bcc     :+
                inc     current_time_value+1
:


        ; Update seconds counter
                inc     jiffies
                lda     jiffies
                cmp     #JIFFIES_PER_SEC
                bne     @done
                lda     #0
                sta     jiffies

        ; Increment seconds
                inc     seconds
                bne     :+
                inc     seconds+1
:
@done:

;                lda     20
;                sta     712

                jmp SYSVBV
  




;-- LICENSE FOR c64timer.s --
; The contents of this file are subject to the Mozilla Public License
; Version 1.1 (the "License"); you may not use this file except in
; compliance with the License. You may obtain a copy of the License at
; http://www.mozilla.org/MPL/
; 
; Software distributed under the License is distributed on an "AS IS"
; basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
; License for the specific language governing rights and limitations
; under the License.
; 
; The Original Code is ip65.
; 
; The Initial Developer of the Original Code is Jonno Downes,
; jonno@jamtronix.com.
; Portions created by the Initial Developer are Copyright (C) 2009
; Jonno Downes. All Rights Reserved.  
; -- LICENSE END --
