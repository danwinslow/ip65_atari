cls
del *.o
del *.lib
del *.com
ca65 a8charconv.s                             -D ATARI -D TCP
ca65 a8timer.s                                -D ATARI -D TCP
ca65 a8print.s                                -D ATARI -D TCP
ca65 a8input.s                                -D ATARI -D TCP
ca65 a8_vt100.s                               -D ATARI 
ca65 ..\ip65\timer.s        -o timer.o        -D ATARI -D TCP
ca65 ..\ip65\icmp.s         -o icmp.o         -D ATARI -D TCP 
ca65 ..\ip65\udp.s          -o udp.o          -D ATARI -D TCP
ca65 ..\ip65\ip.s           -o ip.o           -D ATARI -D TCP
ca65 ..\ip65\eth.s          -o eth.o          -D ATARI -D TCP
ca65 ..\drivers\cs8900a.s   -o cs8900a.o      -D ATARI -D TCP
ca65 ..\ip65\arp.s          -o arp.o          -D ATARI -D TCP
ca65 ..\ip65\ip65.s         -o ip65.o         -D ATARI -D TCP
ca65 dragoncart.s                             -D ATARI -D TCP
ca65 ..\ip65\copymem.s      -o copymem.o      -D ATARI -D TCP
ca65 ..\ip65\config.s       -o config.o       -D ATARI -D TCP
ca65 ..\ip65\printf.s       -o printf.o       -D ATARI -D TCP
ca65 ..\ip65\debug.s        -o debug.o        -D ATARI -D TCP
ca65 ..\ip65\dhcp.s         -o dhcp.o         -D ATARI -D TCP
ca65 ..\ip65\dns.s          -o dns.o -D TCP   -D ATARI 
ca65 ..\ip65\dottedquad.s   -o dottedquad.o   -D TCP -D ATARI 
ca65 ..\ip65\string_utils.s -o string_utils.o -D TCP -D ATARI 
ca65 ..\ip65\arithmetic.s   -o arithmetic.o   -D TCP -D ATARI 
ca65 ..\ip65\telnet.s       -o telnet.o       -D TCP -D ATARI 
ca65 ..\ip65\tcp.s          -o tcp.o          -D TCP -D ATARI 
ca65 ..\ip65\output_buffer.s  -o output_buffer.o          -D TCP -D ATARI 
ar65 a a8.lib *.o    
ca65 -l main.s                                -D ATARI -D TCP
ca65 -l ping_com.s                            -D ATARI -D TCP
ca65 -l ethdump_com.s                         -D ATARI -D TCP
ca65 -l telnet_com.s                          -D ATARI -D TCP
ca65 -l dhcp_com.s                            -D ATARI -D TCP
ca65 -l udptest_com.s                            -D ATARI -D TCP
 
rem cl65 -t atari -C atari.cfg -l test.c -o ethdump.com ethdump_com.o a8.lib -vm -m ethdump_com.map
rem copy ethdump.com "x:\Documents and Settings\dan\Desktop\Development\Mirror\d2"
rem copy /Y ethdump.com "c:\Users\Dan\Desktop\Atari Development\Mirror\d2"
rem cl65 -t atari -C atari.cfg -l test.c -o dhcptest.com dhcp_com.o a8.lib -vm -m dhcp_com.map
rem copy dhcptest.com "x:\Documents and Settings\dan\Desktop\Development\Mirror\d2"
rem copy /Y dhcptest.com "c:\Users\Dan\Desktop\Atari Development\Mirror\d2"
rem cl65 -t atari -C atari.cfg -l test.c -o test.com main.o a8.lib -vm -m main.map
rem copy test.com "x:\Documents and Settings\dan\Desktop\Development\Mirror\d2"
rem copy /Y test.com "c:\Users\Dan\Desktop\Atari Development\Mirror\d2"
rem cl65 -t atari -C atari.cfg -l test.c -o ping.com ping_com.o a8.lib -vm -m ping_com.map
rem copy ping.com "x:\Documents and Settings\dan\Desktop\Development\Mirror\d2"
rem copy /Y ping.com "c:\Users\Dan\Desktop\Atari Development\Mirror\d2"
rem cl65 -t atari -C atari.cfg -l test.c -o telnet.com telnet_com.o a8.lib -vm -m telnet_com.map
rem copy telnet.com "x:\Documents and Settings\dan\Desktop\Development\Mirror\d2"
rem copy /Y telnet.com "c:\Users\Dan\Desktop\Atari Development\Mirror\d2"
cl65 -t atari -C atari.cfg -l test.c -o udptest.com udptest_com.o a8.lib -vm -m udptest_com.map
copy udptest.com "x:\Documents and Settings\dan\Desktop\Development\Mirror\d2"
copy /Y udptest.com "c:\Users\Dan\Desktop\Atari Development\Mirror\d2"


