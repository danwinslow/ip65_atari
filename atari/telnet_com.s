  .include "..\inc\common.i"
  .include "..\inc\commonprint.i"
  .include "..\inc\net.i"
  .include "a8keycodes.i"
  .include "a8.i"
  .include "a8macros.i"

;  .import dns_set_hostname
;  .import dns_resolve
;  .import dns_ip
;  .import dns_status
;  .import cfg_get_configuration_ptr
;
;  .import copymem
;  .importzp copy_src
;  .importzp copy_dest
;  .import icmp_echo_ip
;  .import icmp_ping
;  .import check_for_abort_key
;  .import parse_integer
;  .import strlen
;  .import timer_unhook

  .import _arg1
  .import _arg2
  .import _arg3
  .import _argcount
  .import timer_unhook
  .import get_key_if_available
  .import check_for_abort_key
  .import screen_current_row
  .import screen_current_col
  .import tempa
  .import telnet_connect
  .import telnet_use_native_charset
  .import telnet_port
  .import telnet_ip
  .import dns_set_hostname
  .import dns_resolve
  .import dns_ip
  .import dns_status
  .import cfg_get_configuration_ptr
  .import copymem
  .importzp copy_src
  .importzp copy_dest
  .import parse_integer
  .import strlen
.import GetK
.import OpenKey
.import CloseKey

  
  .export _aip_main
  .export output_buffer
  .export telnet_menu
  .export telnet_on_connection

  .import vt100_init_terminal
  .import vt100_process_inbound_char
  .import vt100_transform_outbound_char
;  .export vt100_init_terminal
;  .export vt100_process_inbound_char
;  .export vt100_transform_outbound_char
;
;  vt100_init_terminal:
;  jsr strout
;  .byte "init terminal",0
;  rts 
;
;  vt100_process_inbound_char:
;    jsr print_a
;  rts
;
;  vt100_transform_outbound_char:
;    ;ldx #0
;    ;jsr print_integer
;    ;jsr print_cr
;    cmp #155
;    bne :+
;    lda #13
;    :
;  rts

  hithere:
  .byte "hi there!",0
  

.bss
  output_buffer: .res 520
  resolved_ip:   .res 4
  loop_ctr:      .res 1
  temp:          .res 1
  tempnum:       .res 2

.CODE

; expects pointer to name in ax
; returns pointer to resolved ip in ax
;         carry set = error
;         carry clear = ok
resolve:
  jsr dns_set_hostname
  jsr dns_resolve
rts

telnet_menu:
rts

telnet_on_connection:
;  jsr strout
;  .byte "got connect!",155,0
rts

_aip_main:

  jsr OpenKey
  
  init_ip_via_dhcp 
  bcc :+             ; branch if no error
  jsr strout
  .byte "ip stack initialization failure.",155,0
  jmp aip_exit
: 
  ;jsr print_ip_config
  jsr strout
  .byte "resolving ",0
  ldax _arg1
  jsr print
  jsr strout
  .byte "...",155,0
    
  ldax _arg1
  jsr resolve
  bcc :+             ; branch if no error
  jsr strout
  .byte "could not resolve ",0
  ldax _arg1
  jsr print
  jmp aip_exit
:

  ; copy ip to telnet
  ldx #$3
:
  lda  dns_ip,x
  sta  telnet_ip,x
  dex
  bpl :-
    
  ;get port
  lda _argcount
  cmp #3
  bcs :+            ; branch on greater than or equal to
    ldy #23         ; __arg is  < 2, default to port 23
    sty telnet_port
    ldy #0
    sty telnet_port+1
    jmp @connect
  :                 ; argc is  >= 2      
  ldax _arg2
  jsr parse_integer   ; currently does not set carry if non-integer in ax
  sta telnet_port     ; so just hope it worked right
  stx telnet_port+1     ; so just hope it worked right

@connect:
  jsr strout
  .byte "connecting to port ",0
  ldax telnet_port
  jsr print_integer
  jsr strout
  .byte " on ",0
  ldax #telnet_ip
  jsr print_dotted_quad
  jsr strout
  .byte 155,0

  lda #255
  sta 764   ; reset keyboard buffer for atari
  ; blah again

;  lda #67
;  sta telnet_ip+0
;  lda #8
;  sta telnet_ip+1
;  lda #168
;  sta telnet_ip+2
;  lda #95
;  sta telnet_ip+3

;  lda #23
;  sta telnet_port

  lda #0
  sta telnet_use_native_charset 

  jsr strout
  .byte "calling connect...",155,0

  jsr telnet_connect

  jsr strout
  .byte "back from connect...",155,0

  jmp aip_exit

@error:
  jsr print_errorcode
  jmp aip_exit

aip_exit:
  jsr timer_unhook
  jsr CloseKey
  rts

