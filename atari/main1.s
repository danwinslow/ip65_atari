; main assembler part
;
; -> (c) 2012 C.Kr�ger <-

.include "atari.inc"

.CODE
.proc Main
		lda	#6
		sta 710
                rts;
END:	
.endproc
		
.SEGMENT "EXEHDR"
.word	$FFFF
.word	Main
.word	Main::END - 1

.segment "AUTOSTRT"
.word	RUNAD			; defined in atari.h
.word	RUNAD+1
.word	Main

