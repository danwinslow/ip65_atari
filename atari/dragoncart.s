;Atari Dragon Cart driver

	.export cs_init

	.export cs_packet_page
	.export cs_packet_data
	.export cs_rxtx_data
	.export cs_tx_cmd
	.export cs_tx_len
	.export eth_driver_name
        .export eth_driver_io_base
  	
cs_rxtx_data	= $D500+$0 ;address of 'recieve/transmit data' port on Dragoncart
cs_tx_cmd	= $D500+$4 ;address of 'transmit command' port on Dragoncart
cs_tx_len	= $D500+$6 ;address of 'transmission length' port on Dragoncart
cs_packet_page	= $D500+$a ;address of 'packet page' port on Dragoncart
cs_packet_data	= $D500+$c ;address of 'packet data' port on Dragoncart
 

	.code

cs_init:
	
	rts

.rodata
eth_driver_name:
	.byte "DRAGONCART",0
eth_driver_io_base:
	.word $D500


;-- LICENSE FOR dragoncart.s --
; The contents of this file are subject to the Mozilla Public License
; Version 1.1 (the "License"); you may not use this file except in
; compliance with the License. You may obtain a copy of the License at
; http://www.mozilla.org/MPL/
; 
; Software distributed under the License is distributed on an "AS IS"
; basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
; License for the specific language governing rights and limitations
; under the License.
; 
; The Original Code is ip65.
; 
; The Initial Developer of the Original Code is Jonno Downes,
; jonno@jamtronix.com.
; Portions created by the Initial Developer are Copyright (C) 2009
; Jonno Downes. All Rights Reserved.  
; -- LICENSE END --
