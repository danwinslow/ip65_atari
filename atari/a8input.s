.export get_key  
.export get_filtered_input
.export filter_text
.export filter_ip
.export filter_dns
.export filter_url
.export filter_number
.export check_for_abort_key
.export get_key_if_available
.export get_key_ip65
.export GetK
.export OpenKey
.export CloseKey

.importzp copy_src

.import ip65_process

KCHAN = $40 ;IOCB to use for keyboard input
temp_key:
.byte 0

.include "../inc/common.i"
.include "a8.i"
.code

allowed_ptr=copy_src ;reuse zero page

;GetK - Waits for and get a keypress (with no screen echo).
; Parameters: None
; Returns: key in accum
; Notes: Must call OpenKey before use.
GetK:
  ldx #KCHAN
  lda #7 ;get
  sta $342,x
  lda #0
  sta $348,x
  sta $349,x
  jmp CIOV

  .data
kname: .byte "K:",155
   .code

;OpenKey - Opens a channel to the keyboard
; Parameters: None
; Returns: carry set and y>127 on error.
; Notes: Must call this before using GetK.
OpenKey:
  jsr CloseKey
  ldx #KCHAN
  lda #3
  sta $342,x
  lda #< kname
  sta $344,x
  lda #> kname
  sta $345,x
  lda #4
  sta $34A,x
  jmp CIOV

;CloseKey - Close channel to keyboard
; Parameters: None
; Returns: carry set and y>127 on error.
; Notes: Use to close keyboard if OpenK was used.
CloseKey:
  ldx #KCHAN
  lda #12
  sta $342,x
  jmp CIOV

;inputs: none
;outputs: A contains ASCII value of key just pressed
get_key:
  jsr get_key_if_available
  beq get_key
  rts

;inputs: none
;outputs: A contains ASCII value of key just pressed (0 if no key pressed)
get_key_if_available:
  lda 764
  cmp #255
  beq @nokey
  jsr GetK
  sta temp_key
  lda #255
  sta 764   ; reset keyboard buffer for atari
  lda temp_key
  rts
@nokey:
  lda #0
  rts

;use ATARI CIOV function to read 1 char
;inputs: A should be set to ASCII char to display
;outputs: none
read_a:
  sta temp_key
  txa
  pha
  tya
  pha
  LDA #7
  STA ICCOM
  LDA #1			
  STA ICBLL
  LDA #0			
  STA ICBLH
  lda #<temp_key
  STA ICBAL
  lda #>temp_key
  STA ICBAH
  LDX #0
  JSR CIOV
  pla
  tay
  pla
  tax
rts

;process inbound ip packets while waiting for a keypress
get_key_ip65:
  jsr ip65_process
  jsr get_key_if_available
  beq get_key_ip65
  rts


;check whether the RUN/STOP key is being pressed
;inputs: none
;outputs: sec if RUN/STOP pressed, clear otherwise
check_for_abort_key:
jmp @not_abort
  lda $cb ;current key pressed
  cmp #$3F
  bne @not_abort
@flush_loop:
  jsr get_key_if_available
  bne @flush_loop
  lda $cb ;current key pressed
  cmp #$3F
  beq @flush_loop
  sec
  rts
@not_abort:
  clc
  rts

;cribbed from http://codebase64.org/doku.php?id=base:robust_string_input
;======================================================================
;Input a string and store it in GOTINPUT, terminated with a null byte.
;AX is a pointer to the allowed list of characters, null-terminated.
;set AX to $0000 for no filter on input
;max # of chars in y returns num of chars entered in y.
;======================================================================


; Main entry
get_filtered_input:
  sty MAXCHARS
  stax temp_allowed

  ;Zero characters received.
  lda #$00
  sta INPUT_Y

;Wait for a character.
@input_get:
  jsr get_key
  sta LASTCHAR

  cmp #$14               ;Delete
  beq @delete

  cmp #$0d               ;Return
  beq @input_done

  ;End reached?
  lda INPUT_Y
  cmp MAXCHARS
  beq @input_get

  ;Check the allowed list of characters.
  ldax temp_allowed
  stax allowed_ptr  ;since we are reusing this zero page, it may not stil be the same value since last time!

  ldy #$00
  lda allowed_ptr+1     ;was the input filter point nul?
  beq @input_ok
@check_allowed:
  lda (allowed_ptr),y           ;Overwritten
  beq @input_get         ;Reached end of list (0)

  cmp LASTCHAR
  beq @input_ok           ;Match found

  ;Not end or match, keep checking
  iny
  jmp @check_allowed

@input_ok:
  lda LASTCHAR          ;Get the char back
  ldy INPUT_Y
  sta GOTINPUT,y        ;Add it to string
  jsr $ffd2             ;Print it

  inc INPUT_Y           ;Next character

  ;Not yet.
  jmp @input_get

@input_done:
   ldy INPUT_Y
   beq  @no_input
   lda #$00
   sta GOTINPUT,y   ;Zero-terminate
   clc
   ldax #GOTINPUT
   rts
@no_input:
   sec
   rts
; Delete last character.
@delete:
  ;First, check if we're at the beginning.  If so, just exit.
  lda INPUT_Y
  bne @delete_ok
  jmp @input_get

  ;At least one character entered.
@delete_ok:
  ;Move pointer back.
  dec INPUT_Y

  ;Store a zero over top of last character, just in case no other characters are entered.
  ldy INPUT_Y
  lda #$00
  sta GOTINPUT,y

  ;Print the delete char
  lda #$14
  jsr $ffd2

  ;Wait for next char
  jmp @input_get


;=================================================
;Some example filters
;=================================================

filter_text:
  .byte ",!#'()* "
filter_url: 
.byte ":/%&?+$"
filter_dns:
.byte "-ABCDEFGHIJKLMNOPQRSTUVWXYZ"
filter_ip:
.byte "."
filter_number: 
.byte "1234567890",0

;=================================================
.bss
temp_allowed: .res 2
MAXCHARS: .res 1
LASTCHAR: .res 1
INPUT_Y: .res 1  
GOTINPUT: .res 40



;-- LICENSE FOR c64inputs.s --
; The contents of this file are subject to the Mozilla Public License
; Version 1.1 (the "License"); you may not use this file except in
; compliance with the License. You may obtain a copy of the License at
; http://www.mozilla.org/MPL/
; 
; Software distributed under the License is distributed on an "AS IS"
; basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
; License for the specific language governing rights and limitations
; under the License.
; 
; The Original Code is ip65.
; 
; The Initial Developer of the Original Code is Jonno Downes,
; jonno@jamtronix.com.
; Portions created by the Initial Developer are Copyright (C) 2009
; Jonno Downes. All Rights Reserved.  
; -- LICENSE END --
